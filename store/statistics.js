export const state = () => ({
    baseStatistics: [],
    exercisesSeries: [],
    markSeries: [],
})

export const mutations = {
    setBaseStatistics(state, baseStatistics) {
        state.baseStatistics = baseStatistics
    },
    setExercisesSeries(state, exercisesSeries) {
        state.exercisesSeries = exercisesSeries
    },
    setMarkSeries(state, markSeries) {
        state.markSeries = markSeries
    }
}