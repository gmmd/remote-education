import axios from 'axios'

export const state = () => ({
    faculties: [],
    departments: [],
    teacherId: null,
    groups: [],
    subjects: [],
    title: ''
})

export const getters = {
    getDepartment: state => id => {
        let res = state.departments.filter(item => item.id == id)
        return res[0] ? res[0] : {};
    },
    getFaculty: state => id => {
        let res = state.faculties.filter(item => item.id == id)
        return res[0] ? res[0] : {};
    },
    getFacultyForSelect(state) {
        return state.faculties.map(item => {
            return {
                value: item.id,
                text: item.name,
                facultyId: item.facultyId
            };
        });
    },
    getTeacherId(state) {
        return state.teacherId
    }
}

export const mutations = {
    setFaculties(state, faculties) {
        state.faculties = faculties
    },
    setDepartments(state, departments) {
        state.departments = departments
    },
    setTeacherId(state, id) {
        localStorage.setItem('teacherId', JSON.stringify(id))
        state.teacherId = localStorage.getItem('teacherId')
    },
    setGroups(state, groups) {
        state.groups = groups
    },
    setTitle(state, title) {
        state.title = title
    },
    setSubjects(state, subjects) {
        state.subjects = subjects
    },
}

export const actions = {
    getFaculties({ commit }) {
        let instance = axios.create({
            baseURL: process.env.API_URL
        })
        instance.get("/api/faculty/university/1").then(response => {
            commit("setFaculties", response.data.data);
        });
    },
    getDepartments({ commit }) {
        let instance = axios.create({
            baseURL: process.env.API_URL
        })
        instance.get("/api/department").then(response => {
            commit("setDepartments", response.data.data);
        });
    }
}