export const actions = {
    nuxtClientInit({ dispatch }, context) {
        dispatch('collections/getFaculties')
        dispatch('collections/getDepartments')
    }
}