import axios from 'axios'
import JWT from "jwt-decode"

export const state = () => ({
    accessToken: localStorage.getItem('accessToken') || '',
    error: '',
})

export const getters = {}

export const mutations = {
    setAccessToken(state, token) {
        state.accessToken = token
    },
    setError(state, error) {
        state.error = error
    },
}

export const actions = {
    login({ commit }, token) {
        let user = JWT(token)
        localStorage.setItem('role', user.system_role);
        localStorage.setItem(`${user.system_role}Id`, user[`${user.system_role}_id`]);
        localStorage.setItem('accessToken', token);
        commit('setAccessToken', token)
        return true
    },
    logout({ commit }) {
        localStorage.clear('accessToken');
        commit('setAccessToken', "")
        return true
    }
}