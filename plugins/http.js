import axios from 'axios'

export default ({ app, store, redirect }, inject) => {
    let instance = axios.create({
        baseURL: process.env.API_URL
    })
    instance.interceptors.request.use((request) => {
        if (store.state.user.accessToken)
            request.headers.Authorization = 'Bearer ' + store.state.user.accessToken
        return request
    })
    instance.interceptors.response.use(
        response => {
            return response
        },
        error => {
            if (error.response && error.response.status == 401) {
                store.dispatch('user/logout').then(() => {
                    redirect('/login')
                })
            } else if (error.response && error.response.status == 400) {
                if (error.response.data.errors) {
                    Object.keys(error.response.data.errors).forEach(errorMsg => {
                        app.$toast.error(error.response.data.errors[errorMsg]).goAway(5000)
                    })
                } else {
                    app.$toast.error(error.response.data.errorMessage.ru).goAway(5000)
                }
            }
            return Promise.reject(error)
        })
    inject('http', instance)
}