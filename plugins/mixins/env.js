import Vue from 'vue'

Vue.mixin({
    data() {
        return {
            env: {
                API_URL: process.env.API_URL
            }
        }
    }
})