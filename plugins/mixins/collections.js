import Vue from 'vue'
import { mapGetters } from 'vuex'

Vue.mixin({
    computed: {
        ...mapGetters({
            getFaculty: 'collections/getFaculty',
            getDepartment: 'collections/getDepartment',
            faculties: 'collections/getFacultyForSelect',
            teacherId: 'collections/getTeacherId'
        })
    },
})