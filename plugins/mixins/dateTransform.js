import Vue from 'vue'

Vue.mixin({
    methods: {
        dateToISO(date) {
            let ISO = date;
            if (ISO) {
                let date = ISO.split("/");

                [date[0], date[2]] = [date[2], date[0]];
                ISO = date.join("-") + "T08:00:00.000Z";
            }
            return ISO;
        },
        ISOtoDate(ISO) {
            let date = ISO.split("-");
            date[2] = date[2].slice(0, -9);
            date = date.reverse();
            // [date[0], date[1]] = [date[1], date[0]];
            date = date.join("/");

            return date
        }
    }
})