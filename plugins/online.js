import axios from 'axios'

export default ({ store }) => {
    let instance = axios.create({
        baseURL: process.env.API_URL

    })

    instance.interceptors.request.use((request) => {
        if (store.state.user.accessToken)
            request.headers.Authorization = 'Bearer ' + store.state.user.accessToken
        return request
    })

    setInterval(() => {
        instance.get(`/api/statistics/ping`).then(response => {})
    }, 3 * 60 * 1000)

    setInterval(() => {
        instance.get(`/api/statistics/day/ping`).then(response => {})
    }, 12 * 60 * 60 * 1000)

}