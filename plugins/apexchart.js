import Vue from 'vue';
import VueApexCharts from 'vue-apexcharts';
import Ru from 'apexcharts/dist/locales/ru.json';

Vue.use(Ru)
Vue.component('apexchart', VueApexCharts)