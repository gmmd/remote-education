var webpack = require('webpack')
var path = require('path')

export default {
    server: {
        port: 3000,
        host: "0.0.0.0"
    },
    ssr: false,
    /*
     ** Headers of the page
     */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' }
        ],
        script: []
    },
    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: '#fff'
    },
    /*
     ** Global CSS
     */
    css: [
        'assets/css/main.scss',
        '~/node_modules/vue-wysiwyg/dist/vueWysiwyg.css',
    ],
    router: {
        middleware: ['auth']
    },
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '~/plugins/http',
        '~/plugins/mixins/env',
        '~/plugins/mixins/collections',
        '~/plugins/mixins/dateTransform',
        '~/plugins/mask',
        '~plugins/apexchart',
        '~plugins/online',
        { src: '~/plugins/timer', mode: 'client' },
        { src: '~/plugins/wysiwyg', mode: 'client' },
        { src: '~plugins/ga.js', mode: 'client' }
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://bootstrap-vue.js.org
        'bootstrap-vue/nuxt',
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        // Doc: https://github.com/nuxt-community/dotenv-module
        '@nuxtjs/dotenv',
        '@nuxtjs/toast',
        'nuxt-client-init-module',
        'cookie-universal-nuxt', ['nuxt-fontawesome', {
            component: 'fa',
            imports: [
                { set: '@fortawesome/free-solid-svg-icons', icons: ['fas'] },
                { set: '@fortawesome/free-brands-svg-icons', icons: ['fab'] }
            ]
        }]
    ],
    toast: {
        position: 'top-right'
    },
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {},
    /*
     ** Build configuration
     */
    build: {

        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
            plugins: [
                new webpack.ProvidePlugin({
                    '$': 'jquery',
                    jQuery: 'jquery'
                })
            ]
        }
    }
}