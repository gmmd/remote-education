export default function ({
  app,
  redirect,
  route
}) {
  let token = app.store.state.user.accessToken
  let routes = ['login', 'forgot', 'forgot-token', 'admin-auth']
  let role = localStorage.getItem('role')
  let admin = ((role == 'admin') || (role == 'inspector')) ? true : false

  if (routes.indexOf(route.name) === -1) {
    if (!admin && route.name.includes('admin'))
      redirect('/')
    if (admin && (!route.name.includes('admin') && !route.name.includes('subject')))
      redirect('/admin')
    if (role == 'student' && route.name.includes('exams'))
      redirect('/exam')
    if (!token)
      return redirect('/login')
    else
      return;
  } else if (token) {
    return redirect('/')
  }
}
